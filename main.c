#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "cas.h"

#define nullptr ((void*)0)

pthread_mutex_t mutex;
int ready = 0;

void Barrier(int threads) {
  pthread_mutex_lock(&mutex);
  ++ready;
  pthread_mutex_unlock(&mutex);

  while (threads != ready) {
    //pthread_yield();
  }
}

volatile int count = 0;

#define kThreads 10

pthread_t threads[kThreads];

#define kIterations 10000000


void AtomicIncrement(volatile int* var) {
  for (;;) {
    int old_value = *var;
    if (CompareExchangeWeak(var, &old_value, old_value + 1)) {
      break;
    }
  }
}

void* DoWork(void* data) {

  Barrier(kThreads);
  printf("Thread started\n");

  for (size_t i = 0; i < kIterations; ++i) {
    //AtomicIncrement(&count);
    
    int expected = 0;	  
    if (CompareExchangeWeak(&count, &expected, expected) == 0) {
      printf("Weak CAS fault\n");
    }
  }

  return nullptr;
}

void* Adversary(void* data) {
  (void)data;

  Barrier(kThreads);

  for (size_t i = 0; i < kIterations; ++i) {
    ResetExclusiveMonitor();	 
  }

  return NULL;
}

int main() {
  pthread_mutex_init(&mutex, nullptr);

  for (size_t i = 0; i < kThreads; ++i) {
    if (i % 2 == 1) {
      pthread_create(&threads[i], nullptr, DoWork, nullptr);
    } else { 
      pthread_create(&threads[i], nullptr, Adversary, nullptr);
    }      
  }

  for (size_t i = 0; i < kThreads; ++i) {
    pthread_join(threads[i], nullptr);
  }

  printf("count = %d\n", count);

  return 0;
}
