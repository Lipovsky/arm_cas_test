## Dependencies

1. `apt install gcc-arm-linux-gnueabi`
2. `apt install qemu user`

## Run

1. `make build`
2. `make run`

## Links

- [Introducing ARM assembly language](http://www.toves.org/books/arm/#s2.3)

- https://ownyourbits.com/2018/06/13/transparently-running-binaries-from-any-architecture-in-linux-with-qemu-and-binfmt_misc/
- https://github.com/qemu/qemu
- [QEMU: Architecture and Internals](https://www.csd.uoc.gr/~hy428/reading/qemu-internals-slides-apr18-2016.pdf)

- [Parallel QEMU](https://medicineyeh.wordpress.com/2016/02/23/parallel-qemu/)
- [Atomic Instruction Translation towards a Multi-threaded QEMU](http://www.scs-europe.net/dlib/2016/ecms2016acceptedpapers/0587-dis_ECMS_0074.pdf)