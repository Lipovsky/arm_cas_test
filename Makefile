
build:
	arm-linux-gnueabi-gcc main.c cas.S -O0 -mcpu=cortex-a7 -o test_cas -pthread -static

run:
	qemu-arm -cpu cortex-a7 ./test_cas
